import { faker } from '@faker-js/faker';

export { LineUtils } from './line-utils';
export { log as Logger } from './logger';

export enum UserType { Customer, Admin, Guest }

export function getDatetime() {
    console.log('[Exec] getDatetime');
    return new Date();
}

export function getUsers() {
    const users = [];
    for (let i = 0; i < 10; i++) {
        users.push(faker.helpers.createCard())
    }
    return users;
}

// // if(require.main === module) {
// //     // CALL directly
// //     console.log(getUsers());
// // }

// export enum UserType {
//     Admin,
//     User,
//     Guest
// }

// export function a() {
//     console.log('ABC');
// }