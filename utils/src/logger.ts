import logger from 'pino'
import dayjs from 'dayjs'

export const log = logger({
    prettyPrint: true,
    base: {
        pid: false
    },
    level: 'debug',
    timestamp: () => `,"time":"${dayjs().format()}"`
});

// @ts-ignore
log.infoFn = function (msg: string, fn?: string) {
    log.info(`${fn ? "[" + fn + "]" : ""} ${msg}`);
}

// @ts-ignore
log.debugFn = function (msg: string, fn?: string) {
    log.debug(`${fn ? "[" + fn + "]" : ""} ${msg}`);
}
