import axios from 'axios';
import querystring from 'querystring';

export class LineUtils {
    static lineUrl = 'https://notify-api.line.me/api/notify';
    static lineToken: string;
    static lineHeaders = {};

    constructor() { }

    static setHeaders = (accessToken?: string) => {
        LineUtils.lineToken = accessToken || process.env.lineAccessToken as string;
        LineUtils.lineHeaders = {
            'content-type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + LineUtils.lineToken
        }
    }

    static notify = (message: string, accessToken?: string) => {
        LineUtils.setHeaders(accessToken);
        return axios.post(LineUtils.lineUrl, querystring.stringify({ message }), { headers: LineUtils.lineHeaders }).then(x => x.data);
    }
}