import { getDataFromFa } from './get-data-from-fa';
import { syncMasterfile } from './sync-masterfile';
export { getDataFromFa, syncMasterfile }