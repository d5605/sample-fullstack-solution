import * as path from 'path';
import {LineUtils, Logger} from 'utils';

const fn = path.basename(__filename).replace(path.extname(__filename), '');

export async function getDataFromFa(){
    Logger.info(`[EXEC] getDataFromFa()`);
    if(process.env.LINE_NOTI_TOKEN){
        await LineUtils.notify('[EXEC] getDataFromFa() ' + new Date(), process.env.LINE_NOTI_TOKEN);
    }
}