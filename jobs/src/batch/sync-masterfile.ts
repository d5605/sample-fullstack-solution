import * as path from 'path';
import {LineUtils, Logger} from 'utils';

const fn = path.basename(__filename).replace(path.extname(__filename), '');

export async function syncMasterfile(){
    Logger.info(`[EXEC] syncMasterfile()`);
    if(process.env.LINE_NOTI_TOKEN){
        await LineUtils.notify(`[EXEC] syncMasterfile() ' + new Date()`, process.env.LINE_NOTI_TOKEN);
    }
}