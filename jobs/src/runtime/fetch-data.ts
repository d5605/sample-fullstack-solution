import * as path from 'path';
import { LineUtils, Logger } from 'utils';

const fn = path.basename(__filename).replace(path.extname(__filename), '');

export async function fetchData() {
    setInterval(async () => {
        Logger.info(`[EXEC] fetchData()`);
        if (process.env.LINE_NOTI_TOKEN) {
            await LineUtils.notify('[EXEC] fetchData() ' + new Date(), process.env.LINE_NOTI_TOKEN);
        }
    }, 10000);
}