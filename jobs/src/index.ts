import { Logger } from 'utils';
import * as batch from './batch';
import * as runtime from './runtime';

const procType = process.argv.slice(2)[0]; // "batch"|"runtime"
const procName = process.argv.slice(3)[0];

(
    async () => {

        switch (procType) {
            case 'batch':

                switch (procName) {
                    case 'get-data-from-fa':
                        await batch.getDataFromFa();
                    case 'sync-masterfile':
                        await batch.syncMasterfile();
                }

                break;
            case 'runtime':

                switch (procName) {
                    case 'fetch-data':
                        await runtime.fetchData();
                }

                break;
            default:
                console.log('abc');
                Logger.error('No Args');
                process.exit(1);
        }

    }
    
)();

