import express from 'express';
import * as u from 'utils';

const app = express();

app.get('/', (req, res) => res.send('OK'));
app.get('/time', (req, res) => res.json(u.getDatetime()));
app.get('/users', (req, res) => res.json(u.getUsers()));

export default app;